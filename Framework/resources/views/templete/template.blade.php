@yield('head')
<body>
<div class="page-wrapper">

    <div class="wrapper">
        @yield('menu')

        <div class="main-panel">

            @yield('navbar')

            <div class="content">
                @yield('contenido')
            </div>

        </div>
    </div>


</div>
@yield('js')