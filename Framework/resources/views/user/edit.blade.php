
@extends('templete.template')
@extends('comun.head')

@section('head_custom')
    {!! Html::style('plugins/jquery-ui/jquery-ui.css') !!}

@stop

@extends('comun.navbar')
@extends('comun.menu')

@section('contenido')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Edit User</h4>
                        </div>
                        <div class="content">

                            {!!Form::model($usuario,['route'=>['users.update',$usuario], 'method'=>'PUT', 'id'=>'perfil-form', 'files' => true])!!}
                                @include('user.form')
                                {!!Form::hidden('age_user', $usuario->age,['id'=>'age_user'])!!}
                            {!!Form::close()!!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card card-user">
                        <div class="image">
                            <img src="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400" alt="..."/>
                        </div>
                        <div class="content">
                            <div class="author">
                                <a href="#">
                                    {{ Html::image('images/faces/face-7.jpg', 'a picture', array('class' => 'avatar border-gray')) }}

                                    <h4 class="title">{{$usuario['name']}}<br /></h4>
                                </a>
                            </div>
                        </div>
                        <hr>
                        <div class="text-center">
                            <button href="#" class="btn btn-simple"><i class="fa fa-facebook-square"></i></button>
                            <button href="#" class="btn btn-simple"><i class="fa fa-twitter"></i></button>
                            <button href="#" class="btn btn-simple"><i class="fa fa-google-plus-square"></i></button>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>



@stop

@extends('comun.js')

@section('js_custom')

    {!! Html::script('admin/plugins/jquery-ui/jquery-ui.js') !!}

    <!-- bootstrap-wysiwyg -->
    <script>
        $(document).ready(function() {
            $("#age option[value='"+$('#age_user').val()+"']").prop('selected', true);
        });
    </script>



@stop
