        <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label>Name</label>
                {!!Form::text('name',null,['class'=>'form-control','placeholder'=> 'First Name' ])!!}.
                @if ($errors->has('name'))
                    <span class="help-block">
                        <strong>{{ $errors->first('name') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label>Age</label>
                <select id="age" name="age">
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="29">29</option>
                    <option value="30">30</option>
                </select>
                @if ($errors->has('age'))
                    <span class="help-block">
                                        <strong>{{ $errors->first('age') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
    </div>

    <button type="submit" class="btn btn-info btn-fill pull-right">Update</button>

    <div class="clearfix"></div>
