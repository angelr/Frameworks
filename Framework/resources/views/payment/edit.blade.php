
@extends('templete.template')
@extends('comun.head')

@section('head_custom')
    {!! Html::style('plugins/jquery-ui/jquery-ui.css') !!}

@stop

@extends('comun.navbar')
@extends('comun.menu')

@section('contenido')

    {!!Form::model($payment,['route'=>['payments.update',$payment], 'method'=>'PUT', 'id'=>'perfil-form', 'files' => true])!!}
        @include('payment.form')
        {!!Form::hidden('user_selected', $payment->user_payment_id,['id'=>'user_selected'])!!}

    {!!Form::close()!!}

@stop

@extends('comun.js')

@section('js_custom')

    {!! Html::script('plugins/jquery-ui/jquery-ui.js') !!}

    <script>

        $(document).ready(function() {

            $("#user_id option[value='"+$('#user_selected').val()+"']").prop('selected', true);

            $( "#date" ).datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: 0,
                dateFormat: "yy-mm-dd",
                yearRange: "0:+5",
            });
        });
    </script>
@stop
