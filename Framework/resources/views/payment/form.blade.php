<div class="x_panel">
    <div class="x_title">
        <div class="clearfix"></div>
    </div>
    <div class="x_content">
        <div class="form-group">
            {!!Form::label('amount', 'Amount', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])!!}

            <div class="col-md-9 col-sm-9 col-xs-12">
                {!! Form::number('amount',  null, ['required', 'min' => 1, 'class' => 'form-control']) !!}
                @if ($errors->has('amount'))
                    <span class="help-block">
                        <strong>{{ $errors->first('amount') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('user', 'User', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])!!}

            <div class="col-md-9 col-sm-9 col-xs-12">
                <select name="user_id" id="user_id" class="form-control" required>
                    @foreach ($users as $value)
                        <option value="{{$value->id }}"> {{$value->name}}</option>
                    @endforeach
                </select>
                @if ($errors->has('user_id'))
                    <span class="help-block">
                        <strong>{{ $errors->first('user_id') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group">
            {!!Form::label('date', 'Date', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])!!}

            <div class="col-md-9 col-sm-9 col-xs-12">
                {!!Form::text('date', null, ['class'=>'form-control', 'id'=>'date', 'readonly'])!!}
                @if ($errors->has('date'))
                    <span class="help-block">
                        <strong>{{ $errors->first('date') }}</strong>
                    </span>
                @endif
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                <button type="submit"   class="btn btn-success">Submit</button>
            </div>
        </div>
    </div>
</div>
