@extends('templete.template')
@extends('comun.head')

@section('head_custom')
    {!! Html::style('plugins/jquery-ui/jquery-ui.css') !!}

@stop

@extends('comun.navbar')
@extends('comun.menu')

@section('contenido')

    {!!Form::open(['route'=>'payments.store', 'method'=>'POST', 'onsubmit' => 'myFunction()', 'class'=>'form-horizontal form-label-left', 'id'=>'login-form', 'files' => true])!!}
    @include('payment.form')
    {!!Form::close()!!}

@stop
@extends('comun.js')
@section('js_custom')

    {!! Html::script('plugins/jquery-ui/jquery-ui.js') !!}

    <script>
        $(document).ready(function() {

            $( "#date" ).datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: 0,
                yearRange: "0:+5",
                dateFormat: "yy-mm-dd"
            });
        });
    </script>





@stop
