@extends('templete.template')
@extends('comun.head')

@section('head_custom')
    {!! Html::style('plugins/dataTables/dataTables.bootstrap.css') !!}

@stop

@extends('comun.navbar')
@extends('comun.menu')

@section('contenido')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <p class="text-muted font-13 m-b-30">
                        {!!link_to_route('payments.create', $title = 'New Payment', $parameters = array(), $attributes = ['class'=>'btn btn-default'])!!}
                    </p>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                            <thead>
                            <tr>
                                <th>Payment number</th>
                                <th>Amount</th>
                                <th>User</th>
                                <th>Date</th>
                                <th>Edit</th>
                                <th>Delete</th>

                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($payments as $value)
                                <tr class="odd gradeX">
                                    <th>{{ $value->payment_id}}</th>
                                    <th>{{ $value->amount}}</th>
                                    <th>{{ $value->name}}</th>
                                    <th>{{ $value->date}}</th>

                                    <td class="center">{!!link_to_route('payments.edit', $title = 'Edit', $parameters = $value->user_payment_id, $attributes = [])!!}</td>
                                    <td class="center alert">
                                        <form action="{{ route('payments.destroy', $value->user_payment_id) }}" method="POST">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-fill pull-left">Delete</button>
                                        </form>
                                    </td>

                                </tr>
                            @endforeach

                            </tbody>

                        </table>

                    </div>


                </div>

            </div>
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
@stop

@extends('comun.js')

@section('js_custom')

    {!! Html::script('plugins/dataTables/jquery.dataTables.js') !!}
    {!! Html::script('plugins/dataTables/dataTables.bootstrap.js') !!}

    <script>
        $(document).ready(function () {
            $('#dataTables-example').dataTable();
        });

    </script>
@stop
