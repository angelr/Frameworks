@section('js')

    <!--   Core JS Files   -->
    {!! Html::script('js/jquery-1.10.2.js') !!}
    {!! Html::script('js/bootstrap.min.js') !!}


    <!--  Checkbox, Radio & Switch Plugins -->
{{--    {!! Html::script('js/bootstrap-checkbox-radio-switch.js') !!}--}}


    <!--  Charts Plugin -->
    {!! Html::script('js/chartist.min.js') !!}


    <!--  Notifications Plugin    -->
    {!! Html::script('js/bootstrap-notify.js') !!}


    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
    {!! Html::script('js/light-bootstrap-dashboard.js') !!}


    <!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
    {!! Html::script('js/demo.js') !!}



    @yield('js_custom')
@stop