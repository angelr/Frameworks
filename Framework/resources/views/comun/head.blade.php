@section('head')
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="en" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <meta name="description" content="Practica de frameworks">
    <meta name="author" content="Angel Rivas">

    <!-- Mobile Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Practica de frameworks">
    <meta name="keywords" content="Practica, frameworks">
    <meta name="Practica de frameworks" content="">

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>

    {!! Html::style('css/pe-icon-7-stroke.css') !!}


    <!-- **Favicon** -->
    <link href="favicon.html" rel="shortcut icon" type="image/x-icon" />
    {{--<link rel="shortcut icon" href="satele.png">--}}


<!-- Bootstrap core CSS     -->
    {!! Html::style('css/bootstrap.css') !!}
    {!! Html::style('css/animate.min.css') !!}
    {!! Html::style('css/light-bootstrap-dashboard.css') !!}
    {!! Html::style('css/demo.css') !!}

    @yield('head_custom')



</head>
@stop