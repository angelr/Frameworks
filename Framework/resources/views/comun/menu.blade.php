@section('menu')
    <div class="sidebar" data-color="blue" data-image="admin/img/sidebar-5.jpg">


        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    Hi
                </a>
            </div>
            <ul class="nav">
                <li>
                    <a href="{{ url('/users') }}">
                        <i class="pe-7s-user"></i>
                        <p>Users</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/favorites') }}">
                        <i class="pe-7s-user"></i>
                        <p>Favorites</p>
                    </a>
                </li>
                <li>
                    <a href="{{ url('/payments') }}">
                        <i class="pe-7s-map-marker"></i>
                        <p>Payments</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

@stop