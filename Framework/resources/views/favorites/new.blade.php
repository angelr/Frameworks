@extends('templete.template')
@extends('comun.head')

@section('head_custom')

@stop

@extends('comun.navbar')
@extends('comun.menu')

@section('contenido')
    {!!Form::open(['route'=>'favorites.store', 'method'=>'POST', 'onsubmit' => 'myFunction()', 'class'=>'form-horizontal form-label-left', 'id'=>'login-form', 'files' => true])!!}
        @include('favorites.form')
    {!!Form::close()!!}
@stop
@extends('comun.js')

@section('js_custom')


<!-- bootstrap-wysiwyg -->
<script>

</script>



@stop
