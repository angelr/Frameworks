<div class="x_panel">
    <div class="x_title">
        <h2>=D</h2>

        <div class="clearfix"></div>
    </div>
    <div class="x_content">

    <div class="form-group">
        {!!Form::label('user_id', 'User', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])!!}
        <div class="col-md-9 col-sm-9 col-xs-12">
        <select name="user_id" id="user_id" class="form-control" required>
            @foreach ($users as $value)
                <option value="{{$value->id }}"> {{$value->name}}</option>
            @endforeach
        </select>
            @if ($errors->has('user_id'))
                <span class="help-block">
                        <strong>{{ $errors->first('user_id') }}</strong>
                    </span>
            @endif
        </div>
    </div>

    <div class="form-group">

        {!!Form::label('user_favorite_id', 'Favourite', ['class' => 'control-label col-md-3 col-sm-3 col-xs-12'])!!}
        <div class="col-md-9 col-sm-9 col-xs-12">
            <select name="user_favorite_id" id="user_favorite_id" class="form-control" required>
                @foreach ($users as $value)
                    <option value="{{$value->id }}"> {{$value->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('user_favorite_id'))
                <span class="help-block">
                        <strong>{{ $errors->first('user_favorite_id') }}</strong>
                    </span>
            @endif
        </div>
    </div>


        <div class="ln_solid"></div>
    <div class="form-group">
        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
            <button type="submit"   class="btn btn-success">Submit</button>
        </div>
    </div>


</div>
    </div>