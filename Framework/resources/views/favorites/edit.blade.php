
@extends('templete.template')
@extends('comun.head')

@section('head_custom')
    {!! Html::style('admin/plugins/jquery-ui/jquery-ui.css') !!}
@stop

@extends('comun.navbar')
@extends('comun.menu')

@section('contenido')

    {!!Form::model($data,['route'=>['favorites.update',$data], 'method'=>'PUT', 'onsubmit' => 'myFunction()', 'class'=>'form-horizontal form-label-left', 'id'=>'login-form', 'files' => true])!!}
        @include('favorites.form')
        {!!Form::hidden('select_user_value', $data->user_id,['id'=>'select_user_value'])!!}
        {!!Form::hidden('select_favorite_value', $data->user_favorite_id,['id'=>'select_favorite_value'])!!}
    {!!Form::close()!!}
@stop

@extends('comun.js')

@section('js_custom')

    {!! Html::script('admin/plugins/jquery-ui/jquery-ui.js') !!}
    <script>
        $(document).ready(function() {
            $("#user_id option[value='"+$('#select_user_value').val()+"']").prop('selected', true);
            $("#user_favorite_id option[value='"+$('#select_favorite_value').val()+"']").prop('selected', true);
        });
    </script>
@stop
