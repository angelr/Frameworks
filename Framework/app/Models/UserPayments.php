<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPayments extends Model
{
    protected $table = 'user_payments';
    protected  $fillable = [
        'payment_id', 'user_id'
    ];
    //
}
