<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\FavoritesCreateRequest;
use App\Models\Favorites;




class FavoritesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = Favorites::select('favorites.id', 'users.name as user_name', 'users2.name as user_favorite_name')
            ->join('users', 'users.id', '=', 'favorites.user_id')
            ->join('users as users2', 'users2.id', '=', 'favorites.user_favorite_id')
            ->get();
        return view('favorites.list', ['favorites' => $post]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = User::All();
        return view('favorites.new', ['users' => $post]);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FavoritesCreateRequest $request)
    {

        Favorites::create([
            'user_id' => $request['user_id'],
            'user_favorite_id' => $request['user_favorite_id'],
        ]);

        return redirect()->route('favorites.index');

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data =  Favorites::where('id',$id)->first();

        $users = User::All();
        return view('favorites.edit', ['data' => $data, 'users' => $users]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FavoritesCreateRequest $request, $id)
    {
        Favorites::findOrFail($id);
        User::findOrFail($request['user_id']);
        User::findOrFail($request['user_favorite_id']);

        Favorites::where('id', $id)
            ->update([
                'user_id' => $request['user_id'],
                'user_favorite_id' => $request['user_favorite_id'],
            ]);

        return redirect()->route('favorites.index');
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Favorites::findOrFail($id);
        $item->delete();

        return redirect()->route('favorites.index');
        //
    }

    // Delete data from table when is delete a user
    public function deleteRecursiveData($id){
        Favorites::where('user_id', $id)
            ->delete();

        Favorites::where('user_favorite_id', $id)
            ->delete();

    }
}
