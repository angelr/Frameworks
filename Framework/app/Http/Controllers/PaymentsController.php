<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\User;
use App\Models\Payments;
use App\Models\UserPayments;
use App\Http\Requests\PaymentCreateRequest;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = UserPayments::select('user_payments.id as user_payment_id', 'users.name', 'users.age', 'payments.amount', 'payments.date', 'payments.id as payment_id')
            ->join('users', 'users.id', '=', 'user_payments.user_id')
            ->join('payments', 'payments.id', '=', 'user_payments.payment_id')
            ->get();

        return view('payment.list', ['payments' => $post]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = User::All();
        return view('payment.new', ['users' => $post]);

        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentCreateRequest $request)
    {
        User::findOrFail($request['user_id']);

        $paymentData = Payments::create([
            'amount' => $request['amount'],
            'date' => $request['date'],
        ]);


        UserPayments::create([
            'payment_id' => $paymentData->id,
            'user_id' => $request['user_id'],
        ]);

        return redirect()->route('payments.index');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::All();

        $post = UserPayments::select('user_payments.id', 'user_payments.user_id as user_payment_id', 'payments.amount', 'payments.date', 'payments.id as payment_id')
            ->where('user_payments.id', '=', $id)
            ->join('payments', 'payments.id', '=', 'user_payments.payment_id')
            ->first();
        return view('payment.edit', ['payment' => $post, 'users' => $users, 'id' => $id]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentCreateRequest $request, $id)
    {
        User::findOrFail($request['user_id']);
        UserPayments::findOrFail($id);

        UserPayments::where('id', $id)
            ->update([
                'user_id' => $request['user_id']
            ]);

        Payments::where('id', $id)
            ->update([
                'amount' => $request['amount'],
                'date' => $request['date']
            ]);
        return redirect()->route('payments.index');
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = UserPayments::findOrFail($id);

        Payments::where('id', $item->payment_id)
            ->delete();

        $item->delete();

        return redirect()->route('payments.index');
        //
    }
    // Delete data from table when is delete a user

    public function deleteRecursiveData($id){

        $post =  UserPayments::where('user_id',$id)
            ->get();

        foreach ($post as $values){
            Payments::where('id', $values->payment_id)
                ->delete();
        }

        UserPayments::where('user_id', $id)
            ->delete();

    }
}
