﻿var map, marker;
function MapsInit() {
    "use strict";


    map = new google.maps.Map(document.getElementById('gmaps-marker'), {
        zoom: 13,
        center: {lat: -12.043333, lng: -77.028333}
    });

}
// marker = new google.maps.Marker({
//     position: {lat: -12.043333, lng: -77.03},
//     map: map,
//     title: 'Vehicle'
// });
var currentId = 0;
var uniqueId = function() {
    return ++currentId;
}

var markers = {};

function createMarker(lat, lng) {
    console.log(lat);
    console.log(lng);

    if (currentId != 0){
        deleteMarker(currentId);
    }
    var id = uniqueId(); // get new id
    marker = new google.maps.Marker({ // create a marker and set id
        id: id,
        position: {lat: parseFloat(lat), lng: parseFloat(lng)},
        map: map,
        animation: google.maps.Animation.DROP
    });
    markers[id] = marker; // cache created marker to markers object with id as its key
    return marker;
}
function deleteMarker(id) {
    var marker = markers[id]; // find the marker by given id
    marker.setMap(null);
}